package edispatcher_kafka

import (
	"github.com/pkg/errors"
	"github.com/segmentio/kafka-go"
	"strconv"
)

// eventIdDefaultSetter - Setter последнего события в канале приема.
//
// Используется по умолчанию.
//
// Данный сеттер по сути определяет, с какого значения необходимо начинать
// чтение очереди событий.
func eventIdDefaultSetter(reader *kafka.Reader, lastEventId string) error {
	lastOffset, err := strconv.ParseInt(lastEventId, 0, 0)
	if nil != err {
		return errors.Wrap(err, "lastEvent should be an Int")
	}

	err = reader.SetOffset(lastOffset + 1)
	if nil != err {
		return errors.Wrap(err, "failed to set offset to Apache Kafka reader")
	}

	return nil
}
