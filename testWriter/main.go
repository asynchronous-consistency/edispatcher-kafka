package main

import (
	edispatcher_kafka "bitbucket.org/asynchronous-consistency/edispatcher-kafka/v3"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
)

func main() {
	channel, _ := edispatcher_kafka.NewEventChannel([]string{"localhost:9092"}, "events", 200)

	ctx := context.Background()
	messages := make([]string, 200)
	for i := 0; i < 200; i++ {
		messages[i] = fmt.Sprintf(`%v`, i)
	}

	for {
		err := channel.Dispatch(ctx, messages)
		if nil != err {
			logrus.WithError(err).Fatal(`Failed to dispatch messages`)
		}
	}
}
