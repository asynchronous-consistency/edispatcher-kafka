package edispatcher_kafka

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
	"github.com/pkg/errors"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
)

// Сервис для работы с событиями на стороне шины
type busEventManager struct {
	reader *kafka.Reader
	logger *logrus.Entry
}

// CommitEvent подтверждает принятие события на стороне шины
func (b busEventManager) CommitEvent(ctx context.Context, event *edispatcher.EventTemporaryData) error {
	err := b.reader.CommitMessages(ctx, event.Origin.(kafka.Message))
	if nil != err {
		b.logger.
			WithError(err).
			WithField("code", 500).
			Error(`Failed to commit message`)
	}

	return errors.Wrap(err, "failed to commit message")
}

// RejectEvent отклоняет событие на стороне шины
func (b busEventManager) RejectEvent(context.Context, *edispatcher.EventTemporaryData) error {
	return nil
}
