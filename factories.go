package edispatcher_kafka

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
	"time"
)

// NewEventChannel реализует фабрику канала доставки сообщений
func NewEventChannel(
	kafkaHosts []string,
	eventsTopic string,
	maxWait uint16,
) (edispatcher.EventChannelInterface, edispatcher.BusEventManagerInterface) {
	return NewEventChannelWithCustomEventIdSetter(
		kafkaHosts,
		eventsTopic,
		maxWait,
		eventIdDefaultSetter,
	)
}

// NewEventChannelWithConsumerGroup реализует фабрику канала доставки сообщений с
// использованием consumer групп от Apache Kafka.
func NewEventChannelWithConsumerGroup(
	kafkaHosts []string,
	eventsTopic string,
	consumerGroup string,
	maxWait uint16,
) (edispatcher.EventChannelInterface, edispatcher.BusEventManagerInterface) {
	return NewEventChannelWithConsumerGroupAndCustomEventIdSetter(
		kafkaHosts,
		eventsTopic,
		consumerGroup,
		maxWait,
		eventIdSkipSetter,
	)
}

// NewEventChannelWithCustomEventIdSetter реализует фабрику канала доставки сообщений
// с кастомным установщиком ID события с которого будет начинаться чтение
func NewEventChannelWithCustomEventIdSetter(
	kafkaHosts []string,
	eventsTopic string,
	maxWait uint16,
	eventIdSetter func(reader *kafka.Reader, lastEventId string) error,
) (channel edispatcher.EventChannelInterface, manager edispatcher.BusEventManagerInterface) {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:   kafkaHosts,
		Topic:     eventsTopic,
		Partition: 0,
		MinBytes:  10e3,
		MaxBytes:  10e8,
		MaxWait:   time.Duration(maxWait) * time.Millisecond,
	})

	producer := &kafka.Writer{
		Addr:         kafka.TCP(kafkaHosts...),
		Topic:        eventsTopic,
		RequiredAcks: kafka.RequireAll,
		WriteTimeout: 5 * time.Second,
		BatchBytes:   10e8,
	}

	logger := logrus.WithField("prefix", "edispatcher-kafka/EventChannel")

	channel = &eventChannel{
		reader:        reader,
		logger:        logger,
		produces:      producer,
		eventIdSetter: eventIdSetter,
	}

	manager = &busEventManagerStub{}

	return
}

// NewEventChannelWithConsumerGroupAndCustomEventIdSetter реализует фабрику канала доставки
// сообщений с кастомным установщиком ID события с которого будет начинаться чтение и обработкой
// consumer групп для Apache Kafka
func NewEventChannelWithConsumerGroupAndCustomEventIdSetter(
	kafkaHosts []string,
	eventsTopic string,
	consumerGroup string,
	maxWait uint16,
	eventIdSetter func(reader *kafka.Reader, lastEventId string) error,
) (channel edispatcher.EventChannelInterface, manager edispatcher.BusEventManagerInterface) {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  kafkaHosts,
		Topic:    eventsTopic,
		GroupID:  consumerGroup,
		MinBytes: 10e3,
		MaxBytes: 10e8,
		MaxWait:  time.Duration(maxWait) * time.Millisecond,
	})

	producer := &kafka.Writer{
		Addr:         kafka.TCP(kafkaHosts...),
		Topic:        eventsTopic,
		RequiredAcks: kafka.RequireAll,
		WriteTimeout: 5 * time.Second,
		BatchBytes:   10e8,
	}

	logger := logrus.WithField("prefix", "edispatcher-kafka/EventChannel")

	channel = &eventChannel{
		reader:        reader,
		logger:        logger,
		produces:      producer,
		eventIdSetter: eventIdSetter,
	}

	manager = &busEventManager{
		reader: reader,
		logger: logger,
	}

	return
}
