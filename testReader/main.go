package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher-kafka/v3"
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"bitbucket.org/go-generics/h"
	"context"
	log "github.com/sirupsen/logrus"
)

func main() {
	channel, manager := edispatcher_kafka.NewEventChannelWithConsumerGroup([]string{"localhost:9092"}, "events", "test", 200)
	messages := make(chan *edispatcher.EventTemporaryData, 50)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		err := channel.Receive(ctx, "-1", messages)
		if nil != err {
			log.WithError(err).Fatal(`Failed to receive messages`)
		}
	}()

	for !h.IsCtxDone(ctx) {
		msg := <-messages

		err := manager.CommitEvent(ctx, msg)
		if nil != err {
			log.WithError(err).Fatal(`Failed to commit message`)
		}

		log.WithField("event", msg).Info(`Received event`)
	}
}
