package edispatcher_kafka

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"bitbucket.org/go-generics/h"
	"context"
	"fmt"
	"github.com/pkg/errors"
	"github.com/segmentio/kafka-go"
	"github.com/sirupsen/logrus"
	"io"
	"time"
)

// Канал для работы с сообщениями. Реализует интерфейс взаимодействия с Apache Kafka
type eventChannel struct {
	reader        *kafka.Reader
	logger        *logrus.Entry
	produces      *kafka.Writer
	eventIdSetter func(reader *kafka.Reader, lastEventId string) error
}

// Dispatch выполняет доставку событий в шину
func (e eventChannel) Dispatch(ctx context.Context, eventsData []string) error {
	ctx = h.ValueOrDefaultFn[context.Context](ctx, context.Background(), h.IsNil[context.Context])

	if 0 == len(eventsData) {
		return nil
	}

	e.logger.WithFields(logrus.Fields{
		"code":   100,
		"events": eventsData,
	}).Info("Sending events to kafka")

	messages := make([]kafka.Message, len(eventsData))
	for i := 0; i < len(eventsData); i++ {
		messages[i] = kafka.Message{
			Value: []byte(eventsData[i]),
		}
	}

	err := e.produces.WriteMessages(ctx, messages...)
	if nil != err {
		e.logger.
			WithError(err).
			WithField("code", 500).
			Error("Failed to send event to kafka")

		return errors.Wrap(err, "failed to send event to Apache Kafka")
	}

	return nil
}

// Receive выполняет получение событий из шины
func (e eventChannel) Receive(ctx context.Context, lastEvent string, events chan *edispatcher.EventTemporaryData) error {
	ctx = h.ValueOrDefaultFn[context.Context](ctx, context.Background(), h.IsNil[context.Context])

	err := e.eventIdSetter(e.reader, lastEvent)
	if nil != err {
		e.logger.
			WithError(err).
			WithField("code", 500).
			Error(`failed to set offset to Apache Kafka reader`)

		return errors.Wrap(err, "failed to set offset to Apache Kafka reader")
	}

	for !h.IsCtxDone(ctx) {
		msg, err := e.reader.FetchMessage(ctx)
		if nil != err {
			// Если очередь пуста, то необходимо подждать пока она наполниться
			if err == io.EOF {
				time.Sleep(500 * time.Millisecond)

				continue
			}

			e.logger.
				WithError(err).
				WithField("code", 500).
				Error(`failed to read message from Apache Kafka`)

			return errors.Wrap(err, "failed to read message from Apache Kafka")
		}

		e.logger.WithFields(logrus.Fields{
			"code":    30001,
			"message": string(msg.Value),
			"offset":  msg.Offset,
		}).Debug("Received message from Apache Kafka")

		events <- &edispatcher.EventTemporaryData{
			Event: edispatcher.StoredEventData{
				Id:   fmt.Sprintf(`%v`, msg.Offset),
				Data: string(msg.Value),
			},
			Origin: msg,
		}
	}

	return nil
}
