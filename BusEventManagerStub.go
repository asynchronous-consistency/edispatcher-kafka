package edispatcher_kafka

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
)

// Подставка для менеджмента сообщений. По сути не нужно коммитить сообщения
// в канале, который расчитан на внешнее хранение ID
type busEventManagerStub struct{}

// CommitEvent подтверждает принятие события на стороне шины
func (b busEventManagerStub) CommitEvent(context.Context, *edispatcher.EventTemporaryData) error {
	return nil
}

// RejectEvent отклоняет событие на стороне шины
func (b busEventManagerStub) RejectEvent(context.Context, *edispatcher.EventTemporaryData) error {
	return nil
}
