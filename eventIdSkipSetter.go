package edispatcher_kafka

import (
	"github.com/segmentio/kafka-go"
)

// Setter последнего события в канале приема.
//
// Позволяет пропустить установку последнего события в шине. Это позволит использовать
// автоматическую установку на уровне шины.
func eventIdSkipSetter(*kafka.Reader, string) error {
	return nil
}
